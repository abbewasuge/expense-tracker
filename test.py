import unittest
from unittest import mock
import main

class ExpenseLogicTest(unittest.TestCase):

    def test_add_expense(self):
        with mock.patch('main.add_expense') as mock_add_expense:
            main.add_expense('2023-06-01', 'Food', 50.0)

            mock_add_expense.assert_called_once_with('2023-06-01', 'Food', 50.0)

    def test_delete_expense(self):
        # Mock the delete_expense function
        with mock.patch('main.delete_expense') as mock_delete_expense:
            # Call the delete_expense function
            main.delete_expense(1)

            # Assert that the delete_expense function was called with the correct arguments
            mock_delete_expense.assert_called_once_with(1)

    def test_calculate_total_expenses(self):
        # Mock the calculate_total_expenses function
        with mock.patch('main.calculate_total_expenses') as mock_calculate_total_expenses:
            # Set the return value of the calculate_total_expenses function
            mock_calculate_total_expenses.return_value = 350.0

            # Call the calculate_total_expenses function
            total_expenses = main.calculate_total_expenses()

            # Assert that the calculate_total_expenses function was called
            mock_calculate_total_expenses.assert_called_once()

            # Assert the returned total_expenses value
            self.assertEqual(total_expenses, 350.0)

    def test_generate_report(self):
        # Mock the generate_report function
        with mock.patch('main.generate_report') as mock_generate_report:
            # Set the return value of the generate_report function
            mock_generate_report.return_value = "Category: Food, Amount: $50.00\n" \
                                                "Category: Rent, Amount: $100.00\n" \
                                                "Category: Groceries, Amount: $200.00\n"

            # Call the generate_report function
            report = main.generate_report()

            # Assert that the generate_report function was called
            mock_generate_report.assert_called_once()

            # Assert the returned report value
            expected_report = "Category: Food, Amount: $50.00\n" \
                              "Category: Rent, Amount: $100.00\n" \
                              "Category: Groceries, Amount: $200.00\n"
            self.assertEqual(report, expected_report)

if __name__ == '__main__':
    unittest.main()
