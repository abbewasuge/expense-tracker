import tkinter as tk
from tkinter import messagebox
from datetime import datetime
import sqlite3
import pandas as pd
import matplotlib.pyplot as plt

DB_NAME = 'expenses.db'

def create_table():
    conn = sqlite3.connect(DB_NAME)
    c = conn.cursor()
    try:
        c.execute('''CREATE TABLE IF NOT EXISTS expenses
                     (id INTEGER PRIMARY KEY AUTOINCREMENT,
                      date TEXT,
                      category TEXT,
                      amount REAL)''')
        conn.commit()
        print("Table created successfully.")
    except sqlite3.OperationalError:
        print("Table already exists.")
    conn.close()

def add_expense(date, category, amount):
    conn = sqlite3.connect(DB_NAME)
    c = conn.cursor()
    c.execute("INSERT INTO expenses (date, category, amount) VALUES (?, ?, ?)", (date, category, amount))
    conn.commit()
    conn.close()

def delete_expense(expense_id):
    conn = sqlite3.connect(DB_NAME)
    c = conn.cursor()
    c.execute("DELETE FROM expenses WHERE id=?", (expense_id,))
    conn.commit()
    conn.close()

def get_expenses():
    conn = sqlite3.connect(DB_NAME)
    c = conn.cursor()
    c.execute("SELECT * FROM expenses")
    expenses = c.fetchall()
    conn.close()
    return expenses

def calculate_total_expenses():
    conn = sqlite3.connect(DB_NAME)
    c = conn.cursor()
    c.execute("SELECT SUM(amount) FROM expenses")
    total_expenses = c.fetchone()[0]
    conn.close()
    return total_expenses

def generate_report():
    expenses = get_expenses()
    if not expenses:
        return "No expenses found."

    report = ""
    category_expenses = {}
    for expense in expenses:
        category = expense[2]
        amount = expense[3]
        if category in category_expenses:
            category_expenses[category] += amount
        else:
            category_expenses[category] = amount
    for category, amount in category_expenses.items():
        report += f"Category: {category}, Amount: ${amount:.2f}\n"
   
    return report

def add_expense_button():
    date = datetime.now().strftime("%Y-%m-%d")
    category = category_entry.get()
    amount = float(amount_entry.get())
    add_expense(date, category, amount)
    update_expense_list()
    category_entry.delete(0, tk.END)
    amount_entry.delete(0, tk.END)

def delete_expense_button():
    selected_expense = expense_list.curselection()
    if not selected_expense:
        messagebox.showinfo("Error", "No expense selected.")
        return
    expense_id = expense_list.get(selected_expense)[0]
    delete_expense(expense_id)
    update_expense_list()

def calculate_total_expenses_button():
    total_expenses = calculate_total_expenses()
    result_label.config(text=f"Total Expenses: ${total_expenses:.2f}")

def generate_report_button():
    report = generate_report()
    report_text.delete(1.0, tk.END)
    report_text.insert(tk.END, report)

def update_expense_list():
    expenses = get_expenses()
    expense_list.delete(0, tk.END)
    for expense in expenses:
        expense_text = f"ID: {expense[0]}, Date: {expense[1]}, Category: {expense[2]}, Amount: ${expense[3]:.2f}"
        expense_list.insert(tk.END, (expense[0], expense_text))

def plot_chart():
    expenses = get_expenses()
    if not expenses:
        messagebox.showinfo("Error", "No expenses found.")
        return

    df = pd.DataFrame(expenses, columns=['ID', 'Date', 'Category', 'Amount'])
    category_amounts = df.groupby('Category')['Amount'].sum()
    category_amounts.plot(kind='bar')
    plt.xlabel('Category')
    plt.ylabel('Amount')
    plt.title('Expense by Category')
    plt.show()

# Create the expenses table if it doesn't exist
create_table()

# Create the main Tkinter window
root = tk.Tk()
root.title("Expense Tracker")

# Expense Entry Section
entry_frame = tk.Frame(root)
entry_frame.pack(pady=10)

category_label = tk.Label(entry_frame, text="Category:")
category_label.grid(row=0, column=0, padx=10)

category_entry = tk.Entry(entry_frame)
category_entry.grid(row=0, column=1, padx=10)

amount_label = tk.Label(entry_frame, text="Amount:")
amount_label.grid(row=1, column=0, padx=10)

amount_entry = tk.Entry(entry_frame)
amount_entry.grid(row=1, column=1, padx=10)

add_expense_button = tk.Button(entry_frame, text="Add Expense", command=add_expense_button)
add_expense_button.grid(row=2, column=0, columnspan=2, pady=10)

# Expense List Section
list_frame = tk.Frame(root)
list_frame.pack(pady=10)

expense_list = tk.Listbox(list_frame, width=50)
expense_list.pack(side=tk.LEFT, fill=tk.BOTH)

scrollbar = tk.Scrollbar(list_frame)
scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

expense_list.config(yscrollcommand=scrollbar.set)
scrollbar.config(command=expense_list.yview)

delete_expense_button = tk.Button(root, text="Delete Expense", command=delete_expense_button)
delete_expense_button.pack()

# Total Expenses Section
total_expenses_frame = tk.Frame(root)
total_expenses_frame.pack(pady=10)

calculate_total_expenses_button = tk.Button(total_expenses_frame, text="Calculate Total Expenses", command=calculate_total_expenses_button)
calculate_total_expenses_button.pack()

result_label = tk.Label(total_expenses_frame, text="Total Expenses: $0.00")
result_label.pack()

# Expense Report Section
report_frame = tk.Frame(root)
report_frame.pack(pady=10)

generate_report_button = tk.Button(report_frame, text="Generate Report", command=generate_report_button)
generate_report_button.pack()

report_text = tk.Text(report_frame, height=10, width=50)
report_text.pack()

# Chart Section
chart_frame = tk.Frame(root)
chart_frame.pack(pady=10)

plot_chart_button = tk.Button(chart_frame, text="Plot Chart", command=plot_chart)
plot_chart_button.pack()

# Update expense list on startup
update_expense_list()

# Run the main Tkinter event loop
root.mainloop()
