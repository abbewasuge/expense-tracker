# Expense Tracker

Expense Tracker is a Python application that helps you track and manage your expenses. It allows you to add expenses, delete expenses, calculate total expenses, generate expense reports, and visualize expenses by category.

## Features

- Add expenses with date, category, and amount
- Delete expenses by choosing the expense and clicking on delete button
- Calculate total expenses
- Generate expense reports
- Plot expense chart by category

## Requirements

- Python 3.9
- See `requirements.txt` for the list of dependencies

## Installation

1. Clone the repository:

   ```shell
   git clone https://github.com/abbehwasuge/expense-tracker.git

    cd expense-tracker

    pip install -r requirements.txt

## Usage

1. Run the application:

   ```shell
   python main.py
    ```
2. The Expense Tracker window will appear.
3. Enter the expense category and amount in the respective fields.
4. Click the "Add Expense" button to add the expense.
5. Use the other buttons to delete expenses, calculate total expenses, generate reports, and plot charts.

## Testing

1. To run the tests, use the following command:

    ```shell
    python -m unittest test.py
    ```

